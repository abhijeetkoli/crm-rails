# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151210145951) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "customers", force: :cascade do |t|
    t.integer  "company_id",    limit: 4
    t.integer  "dealership_id", limit: 4
    t.string   "name",          limit: 255
    t.string   "address1",      limit: 255
    t.string   "address2",      limit: 255
    t.string   "city",          limit: 255
    t.string   "state",         limit: 255
    t.string   "zip",           limit: 255
    t.string   "email",         limit: 255
    t.string   "home_phone",    limit: 255
    t.string   "mobile_phone",  limit: 255
    t.string   "work_phone",    limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "customers", ["company_id"], name: "index_customers_on_company_id", using: :btree
  add_index "customers", ["dealership_id"], name: "index_customers_on_dealership_id", using: :btree

  create_table "dealerships", force: :cascade do |t|
    t.integer  "company_id",        limit: 4
    t.decimal  "lat",                           precision: 10
    t.decimal  "lon",                           precision: 10
    t.string   "name",              limit: 255
    t.string   "address1",          limit: 255
    t.string   "address2",          limit: 255
    t.string   "city",              limit: 255
    t.string   "state",             limit: 255
    t.string   "zip",               limit: 255
    t.string   "phone",             limit: 255
    t.string   "website",           limit: 255
    t.string   "email_crm",         limit: 255
    t.string   "email_service",     limit: 255
    t.string   "ga_tracking",       limit: 255
    t.string   "bing_tracking",     limit: 255
    t.string   "contact_name",      limit: 255
    t.string   "contact_email",     limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "logo_file_name",    limit: 255
    t.string   "logo_content_type", limit: 255
    t.integer  "logo_file_size",    limit: 4
    t.datetime "logo_updated_at"
    t.string   "gapi_client_id",    limit: 255
  end

  add_index "dealerships", ["company_id"], name: "index_dealerships_on_company_id", using: :btree

  create_table "lots", force: :cascade do |t|
    t.integer  "vehicle_id",    limit: 4
    t.integer  "dealership_id", limit: 4
    t.integer  "mileage",       limit: 4
    t.date     "lot_date"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "lots", ["dealership_id"], name: "index_lots_on_dealership_id", using: :btree
  add_index "lots", ["vehicle_id"], name: "index_lots_on_vehicle_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255,   null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "staff_dealerships", force: :cascade do |t|
    t.integer  "staff_id",      limit: 4
    t.integer  "dealership_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "staff_dealerships", ["dealership_id"], name: "index_staff_dealerships_on_dealership_id", using: :btree
  add_index "staff_dealerships", ["staff_id"], name: "index_staff_dealerships_on_staff_id", using: :btree

  create_table "staffs", force: :cascade do |t|
    t.integer  "company_id",          limit: 4
    t.integer  "dealership_id",       limit: 4
    t.string   "first_name",          limit: 255
    t.string   "last_name",           limit: 255
    t.string   "phone",               limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
  end

  add_index "staffs", ["company_id"], name: "index_staffs_on_company_id", using: :btree
  add_index "staffs", ["dealership_id"], name: "index_staffs_on_dealership_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "unique_session_id",      limit: 20
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.string   "name",                   limit: 255
    t.integer  "meta_id",                limit: 4
    t.string   "meta_type",              limit: 255
    t.string   "role",                   limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["meta_id", "meta_type"], name: "index_users_on_meta_id_and_meta_type", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vehicle_histories", force: :cascade do |t|
    t.integer  "vehicle_id",  limit: 4
    t.integer  "customer_id", limit: 4
    t.datetime "on_datetime"
    t.string   "service",     limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "vehicle_histories", ["customer_id"], name: "index_vehicle_histories_on_customer_id", using: :btree
  add_index "vehicle_histories", ["vehicle_id"], name: "index_vehicle_histories_on_vehicle_id", using: :btree

  create_table "vehicles", force: :cascade do |t|
    t.integer  "company_id",        limit: 4
    t.integer  "dealership_id",     limit: 4
    t.integer  "customer_id",       limit: 4
    t.string   "classification",    limit: 255
    t.string   "year",              limit: 255
    t.string   "make",              limit: 255
    t.string   "model",             limit: 255
    t.string   "trim",              limit: 255
    t.string   "vin",               limit: 255
    t.string   "stock_number",      limit: 255
    t.float    "amount_financed",   limit: 24
    t.float    "loan_balance",      limit: 24
    t.float    "payment",           limit: 24
    t.string   "apr",               limit: 255
    t.text     "terms",             limit: 65535
    t.date     "sales_date"
    t.date     "next_service_date"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "vehicles", ["company_id"], name: "index_vehicles_on_company_id", using: :btree
  add_index "vehicles", ["customer_id"], name: "index_vehicles_on_customer_id", using: :btree
  add_index "vehicles", ["dealership_id"], name: "index_vehicles_on_dealership_id", using: :btree

  add_foreign_key "customers", "companies"
  add_foreign_key "customers", "dealerships"
  add_foreign_key "dealerships", "companies"
  add_foreign_key "lots", "dealerships"
  add_foreign_key "lots", "vehicles"
  add_foreign_key "staff_dealerships", "dealerships"
  add_foreign_key "staff_dealerships", "staffs"
  add_foreign_key "staffs", "companies"
  add_foreign_key "staffs", "dealerships"
  add_foreign_key "vehicle_histories", "customers"
  add_foreign_key "vehicle_histories", "vehicles"
  add_foreign_key "vehicles", "companies"
  add_foreign_key "vehicles", "customers"
  add_foreign_key "vehicles", "dealerships"
end
