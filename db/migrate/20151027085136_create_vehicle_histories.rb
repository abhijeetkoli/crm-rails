class CreateVehicleHistories < ActiveRecord::Migration
  def change
    create_table :vehicle_histories do |t|
      t.references :vehicle, index: true, foreign_key: true
      t.references :customer, index: true, foreign_key: true
      t.datetime :on_datetime
      t.string :service

      t.timestamps null: false
    end
  end
end
