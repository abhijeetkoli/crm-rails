class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.references :company, index: true, foreign_key: true
      t.references :dealership, index: true, foreign_key: true
      t.string :name
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :email
      t.string :home_phone
      t.string :mobile_phone
      t.string :work_phone

      t.timestamps null: false
    end
  end
end
