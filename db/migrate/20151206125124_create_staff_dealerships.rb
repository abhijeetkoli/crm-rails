class CreateStaffDealerships < ActiveRecord::Migration
  def change
    create_table :staff_dealerships do |t|
      t.references :staff, index: true, foreign_key: true
      t.references :dealership, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
