class AddClientIdToDealerships < ActiveRecord::Migration
  def change
    add_column :dealerships, :gapi_client_id, :string
  end
end
