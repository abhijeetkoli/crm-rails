class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.references :company, index: true, foreign_key: true
      t.references :dealership, index: true, foreign_key: true
      t.references :customer, index: true, foreign_key: true
      t.string :classification
      t.string :year
      t.string :make
      t.string :model
      t.string :trim
      t.string :vin
      t.string :stock_number
      t.float :amount_financed
      t.float :loan_balance
      t.float :payment
      t.string :apr
      t.text :terms
      t.date :sales_date
      t.date :next_service_date

      t.timestamps null: false
    end
  end
end
