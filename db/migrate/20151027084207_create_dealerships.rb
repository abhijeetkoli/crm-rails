class CreateDealerships < ActiveRecord::Migration
  def change
    create_table :dealerships do |t|
      t.references :company, index: true, foreign_key: true
      t.decimal :lat
      t.decimal :lon
      t.string :name
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :phone
      t.string :website
      t.string :email_crm
      t.string :email_service
      t.string :ga_tracking
      t.string :bing_tracking
      t.string :contact_name
      t.string :contact_email

      t.timestamps null: false
    end
  end
end
