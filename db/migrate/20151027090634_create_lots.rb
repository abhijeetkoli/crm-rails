class CreateLots < ActiveRecord::Migration
  def change
    create_table :lots do |t|
      t.references :vehicle, index: true, foreign_key: true
      t.references :dealership, index: true, foreign_key: true
      t.integer :mileage
      t.date :lot_date

      t.timestamps null: false
    end
  end
end
