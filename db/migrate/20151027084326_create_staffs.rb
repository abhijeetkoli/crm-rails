class CreateStaffs < ActiveRecord::Migration
  def change
    create_table :staffs do |t|
      t.references :company, index: true, foreign_key: true
      t.references :dealership, index: true, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :phone

      t.timestamps null: false
    end
  end
end
