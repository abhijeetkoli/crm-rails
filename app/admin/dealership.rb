ActiveAdmin.register Dealership do
  controller do

    def permitted_params
      params.permit dealership: [:company_id, :name]
    end

    def scoped_collection
      Dealership.unscoped
    end

  end

  index do
    selectable_column
    id_column
    column :name
    actions
  end

  show do |dealership|

    attributes_table do
      row :name
    end

  end


  form :html => { :enctype => "multipart/form-data" } do |f|

    f.inputs "Dealership" do
      f.input :company_id, as: :select, collection: Company.all
      f.input :name
      f.input :gapi_client_id
    end

    f.actions
  end

end
