ActiveAdmin.register Staff do
  controller do

    def permitted_params
      params.permit staff: [:company_id, :first_name, :last_name, :name, :phone, :avatar, dealership_ids: [], user_attributes: [:id, :email, :password, :password_confirmation, :role]]
    end

    def scoped_collection
      Staff.unscoped
    end

  end

  index do
    selectable_column
    id_column
    column :name
    actions
  end

  show do |staff|

    attributes_table do

      row :first_name
      row :last_name
      row :phone
      row :company

      row :avatar do
        image_tag(staff.avatar.url(:thumb))
      end

      panel 'User' do
        attributes_table_for staff.user do
          row :email
          row :role
        end
      end

    end

  end


  form :html => { :enctype => "multipart/form-data" } do |f|

    if f.object.new_record?

      f.inputs for: [:user, (f.object.user || f.object.build_user)] do |user|
        user.input :email
        user.input :password
        user.input :password_confirmation
        user.input :role, as: :select, collection: {Staff: 'staff', Administrator: 'admin'}
      end

    end

    f.inputs "Staff" do

      f.input :company_id, as: :select, collection: Company.all

      f.input :first_name
      f.input :last_name
      f.input :phone
      f.input :avatar, :required => false, :as => :file

      f.input :dealerships, as: :check_boxes, :collection => Dealership.all.map{ |dealership|  [dealership.name, dealership.id] }

    end

    f.actions
  end

end
