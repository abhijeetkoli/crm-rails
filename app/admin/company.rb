ActiveAdmin.register Company do
  controller do

    def permitted_params
      params.permit company: [:name]
    end

    def scoped_collection
      Company.unscoped
    end

  end

  index do
    selectable_column
    id_column
    column :name
    actions
  end

  show do |company|

    attributes_table do
      row :name
    end

  end


  form :html => { :enctype => "multipart/form-data" } do |f|

    f.inputs "Company" do
      f.input :name
    end

    f.actions
  end

end
