class Users::SessionsController < Devise::SessionsController
  layout "empty"
  
  respond_to :json
  
  # protect_from_forgery with: :null_session

  before_filter :configure_sign_in_params, only: [:create]
  skip_filter :verify_signed_out_user, only:[:destroy]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  def destroy
    warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    sign_out
    render :json => { :info => "Logged out" }, :status => 200
  end

  def failure
    render :json => { :error => "Login Credentials Failed" }, :status => 401
  end

  protected

  def configure_sign_in_params
    devise_parameter_sanitizer.for(:sign_in) {|u| u.permit(:login, :email, :password)}    
  end

end
