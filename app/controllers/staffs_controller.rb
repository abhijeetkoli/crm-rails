class StaffsController < ApplicationController
  layout :false
  respond_to :json

  before_action :authenticate_user!, except: [:new, :create, :update, :show]
  before_action :set_staff, only: [:show, :edit, :update, :destroy]

  # GET /staffs.json
  def index

    if params[:search]
      exp = "%#{params[:search]}%"
      @staffs = current_user.meta.company.staffs.where('LOWER(first_name) LIKE ? OR LOWER(last_name) LIKE ? ',exp, exp)
    else
      @staffs = current_user.meta.company.staffs
    end

    render :json => @staffs, :methods => [:avatar_url, :dealership_names]
  end

  # GET /staffs/1.json
  def show
    render json: @staff, :methods => [:avatar_url, :name, :dealerships_access, :dealershipids, :dealership_names]
  end

  # GET /staffs/new
  def new
    @staff = Staff.new
    @staff.build_user
  end

  # GET /staffs/1/edit
  def edit
    render json: @staff
  end

  # POST /staffs.json
  def create

    if params[:file]
      params[:staff][:avatar] = params[:file]
    end

    @staff = Staff.new(create_params)

    @staff.company_id = current_user.meta.company.id

    if @staff.save
      @staff.dealerships.build
      render json: @staff, status: :created
    else
      render json: @staff.errors, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /staffs/1.json
  def update
    if @staff.update(update_params)
      @staff.dealerships.build
      render json: @staff, status: :ok
    else
      render json: @staff.errors, status: :unprocessable_entity
    end
  end

  # DELETE /staffs/1.json
  def destroy
    @staff.destroy
    render head :no_content
  end

  private

  def set_staff
    @staff = Staff.find(params[:id])
    if params[:file]
      params[:staff][:avatar] = params[:file]
    end
  end

  def create_params
    params.require(:staff).permit(:first_name, :last_name, :name, :phone, :avatar, dealership_ids: [], user_attributes: [ :id, :email, :password, :password_confirmation ])
  end

  def update_params
    params.require(:staff).permit(:first_name, :last_name, :name, :phone, :avatar, dealership_ids: [])
  end

  def authenticate_staff
    redirect_to(new_user_session_path) unless current_user.meta_type == "Staff"
  end

 end