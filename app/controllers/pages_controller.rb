class PagesController < ApplicationController
  def search_results
  end

  def lockscreen
    render :layout => "empty"
  end

  def invoice
  end

  def invoice_print
    render :layout => "empty"
  end

  def login
    render :layout => false
  end

  def forgot_password
      render :layout => "empty"
  end

  def register
    render :layout => false
  end

  def internal_server_error
    render :layout => "empty"
  end

  def not_found_error
    render :layout => "empty"
  end

  def empty_page
  end

  def dashboard_google
    render :layout => false
  end

  def customers
    render :layout => false
  end

  def vehicles
    render :layout => false
  end

  def staffs
    render :layout => false
  end

  def staff
    render :layout => false
  end

  def add_user
    render :layout => false
  end

  def dealerships
    render :layout => false
  end

  def dealership
    render :layout => false
  end

  def heat_map
    render :layout => false
  end

  def profile
    @profile_id = current_user.meta_id
    @profile_type = current_user.meta_type
    render :layout => false
  end

  def facebook
    render :layout => false
  end

  def twitter
    render :layout => false
  end

  def virtual_sales_floor
    render :layout => false
  end

end
