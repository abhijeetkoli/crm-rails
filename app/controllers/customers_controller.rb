class CustomersController < ApplicationController
  layout :false
  respond_to :json

  before_action :authenticate_user!, except: [:new, :create, :update, :show]
  before_action :set_customer, only: [:show, :edit, :update, :destroy]

  # GET /customers.json
  def index

    if params[:search]
      exp = "%#{params[:search]}%"
      @customers = current_user.meta.company.customers.where('name LIKE ?', exp)
    else
      @customers = current_user.meta.company.customers
    end

    render :json => @customers
  end

  # GET /customers/1.json
  def show
    render json: @customer
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
    render json: @customer
  end

  # POST /customers.json
  def create

    @customer = Customer.new(create_params)

    @customer.company_id = current_user.meta.company.id

    if @customer.save
      render json: @customer, status: :created
    else
      render json: @customer.errors, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /customers/1.json
  def update
    if @customer.update(update_params)
      render json: @customer, status: :ok
    else
      render json: @customer.errors, status: :unprocessable_entity
    end
  end

  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    render head :no_content
  end

  private

  def set_customer
    @customer = Customer.find(params[:id])
  end

  def create_params
    params.require(:customer).permit(:name, :address1, :address2, :city, :state, :zip, :email, :home_phone, :mobile_phone, :work_phone)
  end

  def update_params
    params.require(:customer).permit(:name, :address1, :address2, :city, :state, :zip, :email, :home_phone, :mobile_phone, :work_phone)
  end

end