class VehiclesController < ApplicationController
  layout :false
  respond_to :json

  before_action :authenticate_user!, except: [:new, :create, :update, :show]
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy]

  # GET /vehicles.json
  def index

    if params[:search]
      exp = "%#{params[:search]}%"
      @vehicles = current_user.meta.company.vehicles.where('classification LIKE ? OR year LIKE ? OR make LIKE ? OR model LIKE ? OR trim LIKE ? OR vin LIKE ? OR stock_number LIKE ?', exp, exp, exp, exp, exp, exp, exp)
    else
      @vehicles = current_user.meta.company.vehicles
    end

    render :json => @vehicles

  end

  # GET /vehicles/1.json
  def show
    render json: @vehicle
  end

  # GET /vehicles/new
  def new
    @vehicle = Vehicle.new
  end

  # GET /vehicles/1/edit
  def edit
    render json: @vehicle
  end

  # POST /vehicles.json
  def create

    @vehicle = Vehicle.new(create_params)

    @vehicle.company_id = current_user.meta.company.id

    if @vehicle.save
      render json: @vehicle, status: :created
    else
      render json: @vehicle.errors, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /vehicles/1.json
  def update
    if @vehicle.update(update_params)
      render json: @vehicle, status: :ok
    else
      render json: @vehicle.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vehicles/1.json
  def destroy
    @vehicle.destroy
    render head :no_content
  end

  private

  def set_vehicle
    @vehicle = Vehicle.find(params[:id])
  end

  def create_params
    params.require(:vehicle).permit(:dealership_id, :customer_id, :classification, :year, :make, :model, :trim, :vin, :stock_number, :amount_financed, :loan_balance, :payment, :apr, :terms, :sales_date)
  end

  def update_params
    params.require(:vehicle).permit(:dealership_id, :customer_id, :classification, :year, :make, :model, :trim, :vin, :stock_number, :amount_financed, :loan_balance, :payment, :apr, :terms, :sales_date)
  end

end