class ValidationsController < ApplicationController
  respond_to :json

  def username
    email = validation_params[:value]
    user = User.find_by_email(email)
    render json: { isValid: !user.present?, value: email}, status: :ok
  end

  def validation_params
    params.require(:validation).permit(:value)
  end
end