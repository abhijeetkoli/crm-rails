class DealershipsController < ApplicationController
  layout :false
  respond_to :json

  before_action :authenticate_user!, except: [:new, :create, :update, :show]
  before_action :set_dealership, only: [:show, :edit, :update, :destroy]

  # GET /dealerships.json
  def index

    if params[:search]
      exp = "%#{params[:search]}%"
      @dealerships = current_user.meta.company.dealerships.where('LOWER(name) LIKE ?', exp)
    else
      @dealerships = current_user.meta.company.dealerships
    end

    render :json => @dealerships, :methods => [:logo_url]
  end

  # GET /dealerships/1.json
  def show
    render json: @dealership, :methods => [:logo_url]
  end

  # GET /dealerships/new
  def new
    @dealership = Dealership.new
  end

  # GET /dealerships/1/edit
  def edit
    render json: @dealership
  end

  # POST /dealerships.json
  def create

    if params[:file]
      params[:dealership][:logo] = params[:file]
    end

    @dealership = Dealership.new(create_params)

    @dealership.company_id = current_user.meta.company.id

    if @dealership.save
      render json: @dealership, status: :created
    else
      render json: @dealership.errors, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /dealerships/1.json
  def update
    if @dealership.update(update_params)
      render json: @dealership, status: :ok
    else
      render json: @dealership.errors, status: :unprocessable_entity
    end
  end

  # DELETE /dealerships/1.json
  def destroy
    @dealership.destroy
    render head :no_content
  end

  private

  def set_dealership
    @dealership = Dealership.find(params[:id])
    if params[:file]
      params[:dealership][:logo] = params[:file]
    end
  end

  def create_params
    params.require(:dealership).permit(:lat, :lon, :name, :address1, :address2, :city, :state, :zip, :phone, :email_crm, :email_service, :website, :contact_name, :contact_email, :ga_tracking, :bing_tracking, :logo)
  end

  def update_params
    params.require(:dealership).permit(:lat, :lon, :name, :address1, :address2, :city, :state, :zip, :phone, :email_crm, :email_service, :website, :contact_name, :contact_email, :ga_tracking, :bing_tracking, :logo)
  end

end