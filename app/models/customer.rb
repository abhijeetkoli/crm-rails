class Customer < ActiveRecord::Base
  belongs_to :company
  belongs_to :dealership
  has_many :vehicles
  has_many :vehicle_histories
  accepts_nested_attributes_for :company, :dealership, :vehicles, :vehicle_histories, allow_destroy: true
end
