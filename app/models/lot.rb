class Lot < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :dealership
  accepts_nested_attributes_for :vehicle, allow_destroy: true
end
