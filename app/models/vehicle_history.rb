class VehicleHistory < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :customer
end
