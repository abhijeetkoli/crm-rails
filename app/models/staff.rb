class Staff < ActiveRecord::Base
  belongs_to :company
  belongs_to :dealership
  has_one :user, as: :meta, dependent: :destroy

  has_many :staff_dealerships, dependent: :destroy
  has_many :dealerships, :through => :staff_dealerships

  accepts_nested_attributes_for :user, :company, :dealership

  attr_accessor :name

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def avatar_url
    avatar.url(:thumb)
  end

  def name
    self.first_name + ' ' + self.last_name
  end

  def dealerships_access
    self.dealerships
  end

  def dealershipids
    self.dealership_ids
  end

  def dealership_names
    self.dealerships.pluck(:name)
  end

end
