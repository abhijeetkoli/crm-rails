class Vehicle < ActiveRecord::Base
  belongs_to :company
  belongs_to :customer
  belongs_to :dealership
  has_many :vehicle_histories
  has_many :lots
  accepts_nested_attributes_for :company, :customer, :dealership, :vehicle_histories, :lots, allow_destroy: true
end
