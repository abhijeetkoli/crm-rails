class Dealership < ActiveRecord::Base
  belongs_to :company
  has_many :customers
  has_many :vehicles
  has_many :lots

  has_many :staff_dealerships, dependent: :destroy
  has_many :staffs, :through => :staff_dealerships

  accepts_nested_attributes_for :company, :customers, :vehicles, :lots, :staffs, allow_destroy: true

  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/missing.png"
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/

  def logo_url
    logo.url(:thumb)
  end

end
