class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :session_limitable

  belongs_to :meta, polymorphic: true

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  before_create :skip_confirmation!
  before_create :set_name
  # after_update :send_password_changed_notification

  ROLES = %w[admin staff]

  def skip_confirmation!
    self.confirmed_at = Time.now
  end

  def set_name
    self.name = self.meta.name
  end

  def role?(base_role)
    ROLES.index(base_role.to_s) <= ROLES.index(role)
  end

  private
  
  def send_password_changed_notification
    name = self.meta.name
    email = self.email
    AppMailer.password_changed_notification(name, email).deliver_now if self.encrypted_password_changed?
  end

end
