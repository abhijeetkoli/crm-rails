class Company < ActiveRecord::Base
  has_many :staffs
  has_many :dealerships
  accepts_nested_attributes_for :staffs, :dealerships
end
