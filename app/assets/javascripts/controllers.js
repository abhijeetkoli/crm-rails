/**
 * INSPINIA - Responsive Admin Theme
 *
 */

var appControllers = angular.module('appControllers', []);

/**
 * MainCtrl - controller
 */
appControllers.controller('MainCtrl', ['$scope', '$rootScope', '$location', 'toaster', 'Auth', function($scope, $rootScope, $location, toaster, Auth){

	$rootScope.user = {};
    $rootScope.userType = null;
    $rootScope.role = null;
    $rootScope.staff = {};
    $rootScope.profile = {};
    $rootScope.dealerships = {};
    $rootScope.dealership = {};
    $rootScope.dealershipID = 0;
    $rootScope.dealershipName = 'Select Dealership ';
    $rootScope.isAuthenticated = false;

    // Auth.currentUser().then(function(user) {
    //     // User was logged in, or Devise returned
    //     // previously authenticated session.
    //     // console.log(user); // => {id: 1, ect: '...'}
    //     if(Auth.isAuthenticated()) {
    //         $rootScope.user = user;
    //         $rootScope.isAuthenticated = true;
    //     }
    //     else {
    //         $location.path('/login');     
    //     }

    // }, function(error) {
    //    $rootScope.isAuthenticated = false;
    //    $location.path('/login');
    // });

	$scope.$on('devise:login', function(event, currentUser) {
        // after a login, a hard refresh, a new tab
        $rootScope.isAuthenticated = true;
    });

    $scope.$on('devise:new-session', function(event, currentUser) {
        // user logged in by Auth.login({...})
        // $location.path('/dashboard');
        $rootScope.isAuthenticated = true;
    });

    $scope.$on('devise:logout', function(event, oldCurrentUser) {
        // $location.path('/dashboard');
        $rootScope.isAuthenticated = false;
    });

    $scope.$on('devise:new-registration', function(event, user) {
        // $location.path('/login')
    });

    $scope.$on('devise:unauthorized', function(event, xhr, deferred) {
        // Disable interceptor on _this_ login request,
        // so that it too isn't caught by the interceptor
        // on a failed login.
        var config = {
            interceptAuth: false
        };

        // Ask user for login credentials
        Auth.login(credentials, config).then(function() {
            // Successfully logged in.
            // Redo the original request.
            return $http(xhr.config);
        }).then(function(response) {
            // Successfully recovered from unauthorized error.
            // Resolve the original request's promise.
            deferred.resolve(response);
        }, function(error) {
            // There was an error logging in.
            // Reject the original request's promise.
            deferred.reject(error);
        });
    });


}]);

appControllers.controller('DashboardController', ['$scope', '$rootScope', '$location', '$http', '$window', 'Auth', 'AppUser', 'Staff', 'Dealership', 'ngAnalyticsService', function($scope, $rootScope, $location, $http, $window, Auth, AppUser, Staff, Dealership, ngAnalyticsService) {

    $scope.avatar_url = null;

    if(Auth.isAuthenticated()){
        $rootScope.isAuthenticated = true;

        Auth.currentUser().then(function(user) {
            $rootScope.user = user;
            $rootScope.userType = user.meta_type;
            $rootScope.role = user.role;

            if($rootScope.userType == 'Staff') {

                Staff.get(user.meta_id).then(function (staff) {
                    $rootScope.profile = staff;
                    $scope.avatar_url = staff.avatarUrl;
                    $scope.dealerships = staff.dealershipsAccess;
                    $rootScope.dealership = $scope.dealerships[0];
                    ngAnalyticsService.setClientId($rootScope.dealership.gapiClientId);
                }, function (error) {

                });

                //Dealership.query().then(function (dealerships) {
                //    $rootScope.dealerships = dealerships;
                //});

            }

        });

    }
    else {
       $rootScope.isAuthenticated = false;
       $location.path('/login'); 
    }

    $scope.logout = function() {
        
        var config = {
            headers: {
                'X-HTTP-Method-Override': 'DELETE'
            }
        };
     
        Auth.logout(config).then(function(oldUser) {
            // $location.path('/login');
            $window.location.href = "/";
        }, function(error) {
            // An error occurred logging out.
        });

    };

    $scope.selectDealership = function(dealership) {
        $rootScope.dealershipID = dealership.id;
        $rootScope.dealershipName = dealership.name;
        $rootScope.dealership = dealership;
        ngAnalyticsService.setClientId($rootScope.dealership.gapiClientId);
    }


}]);

appControllers.controller('RegisterController', ['$scope', '$location', 'Auth', function($scope, $location, Auth) {

    $scope.registerationFailed = false;

    $scope.register = function() {

        var credentials = {
            name: $scope.name,
            email: $scope.email,
            password: $scope.password,
            password_confirmation: $scope.password
        };

        var config = {
            headers: {
                'X-HTTP-Method-Override': 'POST'
            }
        };

        Auth.register(credentials, config).then(function(registeredUser) {
            $location.path('/dashboard');
        }, function(error) {            
            $scope.registerationFailed = true;
            $scope.errors = error;
        });
        
    };

}]);

appControllers.controller('LoginController', ['$scope', '$rootScope', '$state', '$location', '$http', 'Auth', function($scope, $rootScope, $state, $location, $http, Auth) {
        
        $scope.loginFailed = false;

        $scope.login = function() {

            var credentials = {
                email: $scope.email,
                password: $scope.password,
                remember_me: 1,
                // utf8: '✓',
                // authenticity_token: $scope.authenticity_token
            };

            var config = {
                headers: {
                    'X-HTTP-Method-Override': 'POST'
                }
            };

            Auth.login(credentials, config).then(function(user) {
                $rootScope.user = user;
                $state.go("dashboard");
            }, function(error) {
                $scope.loginFailed = true;
                $scope.errors = error;
            });

        };
        
    }]);

appControllers.controller('translateController', ['$translate', '$scope',  function($translate, $scope) {
    $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
    };
}]);


appControllers.controller('ProfileController', ['$scope', '$rootScope', '$state', '$http', 'Upload', 'toaster', 'AppUser', 'Profile', 'StaffUser', 'Staff',  function($scope, $rootScope, $state, $http, Upload, toaster, AppUser, Profile, StaffUser, Staff) {
    $scope.saving = false;

    $scope.data = {
        avatar: null,
        staff: {}
    };

    angular.copy($rootScope.profile, $scope.data.staff);

    $scope.updateProfile = function() {

        $scope.saving = true;

        if ($scope.staff_form.$valid) {

            $scope.data.staff = humps.decamelizeKeys($scope.data.staff);

            StaffUser.editWithAttachment($scope.data, $rootScope.profile.id).then(function (results) {

                Staff.get($rootScope.profile.id).then(function (staff) {
                    $rootScope.profile = staff;
                    angular.copy($rootScope.profile, $scope.data.staff);
                    $scope.saving = false;
                }, function (error) {
                    $scope.saving = false;
                });

                toaster.success({title: 'Profile', body: 'Saved changes!'});
            }, function (error) {
                $scope.saving = false;
                toaster.error({title: 'Profile', body: 'Failed to save changes!'});
            });

        } else {
            $scope.staff_form.submitted = true;
        }

    };

}]);


appControllers.controller('StaffsController', ['$scope', 'Staff',  function($scope, Staff) {
    $scope.searching = true;
    $scope.staffs = [];
    $scope.staffID = 0;

    Staff.query().then(function (results) {

        if(results.length > 0)
            $scope.staffs = results;

        $scope.searching = false;
    }, function (error) {
        // do something about the error
        $scope.searching = false;
    });

    $scope.selectStaff = function(id) {
        $scope.staffID = id;
    };

    $scope.isSelected = function(id) {
        return $scope.staffID == id;
    };

    $scope.searchStaff = function() {

        Staff.query({search: $scope.exp}).then(function (results) {
            $scope.staffs = results;

            if (results.length > 0)
                $scope.saffID = results[0].id;

            $scope.searching = false;
        }, function (error) {
            $scope.searching = false;
        });
    };

}]);

appControllers.controller('StaffController', ['$scope', '$state', '$stateParams', '$http', 'notify', 'Staff',  function($scope, $state, $stateParams, $http, notify, Staff) {

    $scope.saving = false;
    $scope.staffID = $stateParams.id;

    $scope.data = {
        staff: new Staff({})
    };

    if($scope.staffID > 0) {

        Staff.get($scope.staffID).then(function (staff) {
            angular.copy(staff, $scope.data.staff);
            $scope.data.staff.dealershipIds = staff.dealershipids;
        }, function (error) {
        });
    }

    $scope.saveStaff = function() {

        $scope.saving = true;

        if ($scope.staff_form.$valid) {

            if($scope.staffID == 0) {

                $scope.data.staff.create().then(function (results) {
                    $scope.saving = false;
                    $state.go('staffs');
                }, function (error) {
                    $scope.saving = false;
                });

            } else {

                $scope.data.staff.update().then(function (results) {
                    $scope.saving = false;
                    $state.go('staffs');
                }, function (error) {
                    $scope.saving = false;
                });

            }

        } else {
            $scope.staff_form.submitted = true;
            $scope.saving = false;
        }

    };

    $scope.dealershipToggle = function(item, list){
        var idx = list.indexOf(item);
        if(idx > -1) list.splice(idx, 1);
        else list.push(item);
    };

    $scope.dealershipExists = function(item, list){
        return list.indexOf(item) > -1;
    };

}]);

appControllers.controller('NewStaffController', ['$scope', '$state', '$http', 'Upload', 'notify', 'StaffUser',  function($scope, $state, $http, Upload, notify, StaffUser) {

    $scope.saving = false;

    $scope.data = {
        avatar: null,
        staff: {
            first_name: '',
            last_name: '',
            name: '',
            phone: '',
            user_attributes: {
                email: '',
                password: '',
                password_confirmation: ''
            }
        }
    };

    $scope.createStaff = function() {

        $scope.saving = true;

        if ($scope.staff_form.$valid) {

            $scope.data.staff.name = $scope.data.staff.first_name + ' ' + $scope.data.staff.last_name;

            $scope.data.staff = humps.decamelizeKeys($scope.data.staff);

            StaffUser.createWithAttachment($scope.data).then(function(results){
                $scope.saving = false;
                $state.go('staff');
            }, function(error){
                $scope.saving = false;
            });

        } else {
            $scope.staff_form.submitted = true;
        }

    };


}]);


appControllers.controller('DealershipsController', ['$scope', 'Dealership',  function($scope, Dealership) {
    $scope.searching = true;
    $scope.dealerships = [];

    Dealership.query().then(function (results) {
        $scope.dealerships = results;

        if(results.length > 0)
            $scope.dealershipID = results[0].id;

        $scope.searching = false;
    }, function (error) {
        // do something about the error
        $scope.searching = false;
    });

    $scope.selectDealership = function(id) {
        $scope.dealershipID = id;
    };

    $scope.editDealership = function(id) {
        $state.go('dealership', {'id': id});
    };

    $scope.isSelected = function(id) {
        return $scope.dealershipID == id;
    };

    $scope.searchDealership = function() {

        Dealership.query({search: $scope.exp}).then(function (results) {
            $scope.dealerships = results;

            if(results.length > 0)
                $scope.dealershipID = results[0].id;

            $scope.searching = false;
        }, function (error) {
            $scope.searching = false;
        });

    };

}]);

appControllers.controller('DealershipController', ['$scope', '$state', '$stateParams', '$http', 'Upload', 'notify', 'Dealership', 'DealershipUpload',  function($scope, $state, $stateParams, $http, Upload, notify, Dealership, DealershipUpload) {

    $scope.saving = false;
    $scope.dealershipID = $stateParams.id;

    $scope.data = {
        logo: null,
        dealership: {}
    };

    if($scope.dealershipID > 0) {

        Dealership.get($scope.dealershipID).then(function (dealership) {
            angular.copy(dealership, $scope.data.dealership);
            $scope.logoUrl = dealership.logoUrl;
        }, function (error) {

        });
    }

    $scope.saveDealership = function() {

        $scope.saving = true;

        if ($scope.dealership_form.$valid) {

            $scope.data.dealership = humps.decamelizeKeys($scope.data.dealership);

            if($scope.dealershipID == 0) {

                DealershipUpload.createWithAttachment($scope.data).then(function (results) {
                    $scope.saving = false;
                    $state.go('dealerships');
                }, function (error) {
                    $scope.saving = false;
                });

            } else {

                DealershipUpload.editWithAttachment($scope.data, $scope.dealershipID).then(function (results) {
                    $scope.saving = false;
                    $state.go('dealerships');
                }, function (error) {
                    $scope.saving = false;
                });

            }

        } else {
            $scope.dealership_form.submitted = true;
            $scope.saving = false;
        }

    };


}]);


appControllers.controller('CustomersController', ['$scope', 'Customer',  function($scope, Customer) {
    $scope.searching = true;
    $scope.customers = [];

    Customer.query().then(function (results) {
        $scope.customers = results;

        if(results.length > 0)
            $scope.customerID = results[0].id;

        $scope.searching = false;
    }, function (error) {
        // do something about the error
        $scope.searching = false;
    });

    $scope.selectCustomer = function(id) {
        $scope.customerID = id;
    };

    $scope.editCustomer = function(id) {
        $state.go('customer', {'id': id});
    };

    $scope.isSelected = function(id) {
        return $scope.customerID == id;
    };

    $scope.searchCustomer = function() {

        Customer.query({search: $scope.exp}).then(function (results) {
            $scope.customers = results;

            if (results.length > 0)
                $scope.customerID = results[0].id;

            $scope.searching = false;
        }, function (error) {
            $scope.searching = false;
        });
    };

}]);

appControllers.controller('CustomerController', ['$scope', '$state', '$stateParams', '$http', 'notify', 'Customer',  function($scope, $state, $stateParams, $http, notify, Customer) {

    $scope.saving = false;
    $scope.customerID = $stateParams.id;

    $scope.data = {
        customer: new Customer({name: 'Customer Name'})
    };

    if($scope.customerID > 0) {

        Customer.get($scope.customerID).then(function (customer) {
            angular.copy(customer, $scope.data.customer);
            $scope.logoUrl = customer.logoUrl;
        }, function (error) {

        });
    }

    $scope.saveCustomer = function() {

        $scope.saving = true;

        if ($scope.customer_form.$valid) {

            if($scope.customerID == 0) {

                $scope.data.customer.create().then(function (results) {
                    $scope.saving = false;
                    $state.go('customers');
                }, function (error) {
                    $scope.saving = false;
                });

            } else {

                $scope.data.customer.update().then(function (results) {
                    $scope.saving = false;
                    $state.go('customers');
                }, function (error) {
                    $scope.saving = false;
                });

            }

        } else {
            $scope.customer_form.submitted = true;
            $scope.saving = false;
        }

    };


}]);


appControllers.controller('VehiclesController', ['$scope', 'Vehicle',  function($scope, Vehicle) {
    $scope.searching = true;
    $scope.vehicles = [];

    Vehicle.query().then(function (results) {
        $scope.vehicles = results;

        if(results.length > 0)
            $scope.vehicleID = results[0].id;

        $scope.searching = false;
    }, function (error) {
        // do something about the error
        $scope.searching = false;
    });

    $scope.selectVehicle = function(id) {
        $scope.vehicleID = id;
    };

    $scope.editVehicle = function(id) {
        $state.go('vehicle', {'id': id});
    };

    $scope.isSelected = function(id) {
        return $scope.vehicleID == id;
    };

    $scope.searchVehicle = function() {

        Vehicle.query({search: $scope.exp}).then(function (results) {
            $scope.vehicles = results;

            if (results.length > 0)
                $scope.vehicleID = results[0].id;

            $scope.searching = false;
        }, function (error) {
            $scope.searching = false;
        });
    };

}]);

appControllers.controller('VehicleController', ['$scope', '$state', '$stateParams', '$http', 'notify', 'Dealership', 'Customer', 'Vehicle',  function($scope, $state, $stateParams, $http, notify, Dealership, Customer, Vehicle) {

    $scope.saving = false;
    $scope.vehicleID = $stateParams.id;

    $scope.data = {
        dealerships: {},
        customers: {},
        vehicle: new Vehicle({})
    };

    Dealership.query().then(function (dealerships) {
        $scope.data.dealerships = dealerships;
    });

    Customer.query().then(function (customers) {
        $scope.data.customers = customers;
    });

    if($scope.vehicleID > 0) {

        Vehicle.get($scope.vehicleID).then(function (vehicle) {
            angular.copy(vehicle, $scope.data.vehicle);
            $scope.logoUrl = vehicle.logoUrl;
        }, function (error) {

        });
    }

    $scope.saveVehicle = function() {

        $scope.saving = true;

        if ($scope.vehicle_form.$valid) {

            if($scope.vehicleID == 0) {

                $scope.data.vehicle.create().then(function (results) {
                    $scope.saving = false;
                    $state.go('vehicles');
                }, function (error) {
                    $scope.saving = false;
                });

            } else {

                $scope.data.vehicle.update().then(function (results) {
                    $scope.saving = false;
                    $state.go('vehicles');
                }, function (error) {
                    $scope.saving = false;
                });

            }

        } else {
            $scope.vehicle_form.submitted = true;
            $scope.saving = false;
        }

    };


}]);

appControllers.controller('GAController', ['$scope', '$rootScope', 'ngAnalyticsService', function($scope, $rootScope, ngAnalyticsService) {

    $scope.charts = [{
        reportType: 'ga',
        query: {
            metrics: 'ga:sessions',
            dimensions: 'ga:date',
            'start-date': '30daysAgo',
            'end-date': 'yesterday'
        },
        chart: {
            container: 'chart-container-1',
            type: 'LINE',
            options: {
                width: '100%'
            }
        }
    }, {
        reportType: 'ga',
        query: {
            metrics: 'ga:sessions',
            dimensions: 'ga:browser',
            'start-date': '30daysAgo',
            'end-date': 'yesterday'
        },
        chart: {
            container: 'chart-container-2',
            type: 'PIE',
            options: {
                width: '100%',
                is3D: true,
                title: 'Browser Usage'
            }
        }
    }, {
        reportType: 'ga',
        query: {
            metrics: 'ga:sessions',
            dimensions: 'ga:country',
            'start-date': '30daysAgo',
            'end-date': 'yesterday',
            'max-results': 6,
            sort: '-ga:sessions'
        },
        chart: {
            container: 'chart-container-3',
            type: 'PIE',
            options: {
                width: '100%',
                is3D: true,
                pieHole: 4/9,
                title: 'Top Countries'
            }
        }
    }];

    $scope.extraChart = {
        reportType: 'ga',
        query: {
            metrics: 'ga:sessions',
            dimensions: 'ga:date',
            'start-date': '30daysAgo',
            'end-date': 'yesterday',
            //ids: 'ga:81197147' // put your viewID here
        },
        chart: {
            container: 'chart-container-3',
            type: 'LINE',
            options: {
                width: '100%'
            }
        }
    };

    $scope.defaultIds = {
        //ids: 'ga:81197147'
    };

    $scope.queries = [{
        query: {
            //ids: 'ga:81197147',  // put your viewID here
            metrics: 'ga:sessions',
            dimensions: 'ga:city'
        }
    }];

    // if a report is ready
    $scope.$on('$gaReportSuccess', function (e, report, element) {
        //console.log(report, element);
    });
}]);