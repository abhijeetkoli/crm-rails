// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery/jquery-2.1.1.js
//= require bootstrap-sprockets
//= require metisMenu/jquery.metisMenu.js
//= require pace/pace.min.js
//= require peity/jquery.peity.min.js
//= require slimscroll/jquery.slimscroll.min.js
//= require inspinia.js

//= require flot/jquery.flot.js
//= require flot/jquery.flot.tooltip.min.js
//= require flot/jquery.flot.resize.js
//= require flot/jquery.flot.pie.js
//= require flot/jquery.flot.time.js
//= require flot/jquery.flot.spline.js
//= require sparkline/jquery.sparkline.min.js
//= require jvectormap/jquery-jvectormap-2.0.2.min.js
//= require jvectormap/jquery-jvectormap-world-mill-en.js
//= require humps/humps.js

//= require angular
//= require angular-animate
//= require angular-aria
//= require angular-route
//= require angular-resource
//= require angular-sanitize
//= require angular-messages
//= require angular-touch

//= require angularjs/rails/resource
//= require angularjs/rails/resource/extensions/snapshots

//= require angular-translate/angular-translate.min.js
//= require oclazyload/dist/ocLazyLoad.min.js
//= require ui-router/angular-ui-router.js
//= require ui-bootstrap/ui-bootstrap-tpls-0.12.0.min.js
//= require ui-bootstrap/angular-bootstrap-checkbox.js
//= require devise/angular-devise.js
//= require ng-file-upload/ng-file-upload-all.min.js
//= require angular-notify/angular-notify.min.js
//= require toastr/toastr.min.js
//= require angular-validation-match/angular-validation-match.js
//= require ng-remote-validate/ngRemoteValidate.js
//= require ng-analytics/ng-analytics.js

