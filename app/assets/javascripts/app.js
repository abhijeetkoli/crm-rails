//= require controllers.js
//= require directives.js
//= require resources.js

/**
 * INSPINIA - Responsive Admin Theme
 *
 */
var app;

app = angular.module('inspinia', [
    'ui.router',                    // Routing
    'oc.lazyLoad',                  // ocLazyLoad
    'ui.bootstrap',                 // Ui Bootstrap
    'Devise',						// Devise
    'pascalprecht.translate',        // Translate
    'appControllers',
    'appResources',
    'inspiniaDirectives',
    'ngFileUpload',
    'cgNotify',
    'toaster',
    'validation.match',
    'remoteValidation',
    'ngAnalytics'
]);

app.run(['ngAnalyticsService', function (ngAnalyticsService) {
    //ngAnalyticsService.setClientId('795959755447-9ioukte5ok5j1sjg3k4nn2ba6vsbp6ru.apps.googleusercontent.com');
}]);

app.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

	$urlRouterProvider.otherwise("/dashboard");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('login', {
            url: "/login",
            templateUrl: "/pages/login",
        })
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "/dashboard",
        })
        .state('dashboard_google', {
            url: "/dashboard_google",
            templateUrl: "/pages/dashboard_google",
            parent: "dashboard"
        })
        .state('virtual_sales_floor', {
            url: "/virtual_sales_floor",
            templateUrl: "/pages/virtual_sales_floor",
            parent: "dashboard"
        })
        .state('heat_map', {
            url: "/heat_map",
            templateUrl: "/pages/heat_map",
            parent: "dashboard"
        })
        .state('facebook', {
            url: "/facebook",
            templateUrl: "/pages/facebook",
            parent: "dashboard"
        })
        .state('twitter', {
            url: "/twitter",
            templateUrl: "/pages/twitter",
            parent: "dashboard"
        })
        .state('staffs', {
            url: "/staffs",
            templateUrl: "/pages/staffs",
            parent: "dashboard"
        })
        .state('staff', {
            url: "/staff/:id",
            templateUrl: "/pages/staff",
            parent: "dashboard"
        })
        .state('newstaff', {
            url: "/newstaff",
            templateUrl: "/pages/add_user",
            parent: "dashboard"
        })
        .state('profile', {
            url: "/profile",
            templateUrl: "/pages/profile",
            parent: "dashboard"
        })
        .state('dealerships', {
            url: "/dealerships",
            templateUrl: "/pages/dealerships",
            parent: "dashboard"
        })
        .state('dealership', {
            url: "/dealership/:id",
            templateUrl: "/pages/dealership",
            parent: "dashboard"
        })
        .state('customers', {
            url: "/customers",
            templateUrl: "/pages/customers",
            parent: "dashboard"
        })
        .state('customer', {
            url: "/customer/:id",
            templateUrl: "/pages/customer",
            parent: "dashboard"
        })
        .state('vehicles', {
            url: "/vehicles",
            templateUrl: "/pages/vehicles",
            parent: "dashboard"
        })
        .state('vehicle', {
            url: "/vehicle/:id",
            templateUrl: "/pages/vehicle",
            parent: "dashboard"
        })
        ;

}]);

app.config(['$httpProvider', function($httpProvider) {
    // $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);

app.config(['$translateProvider',  function($translateProvider) {

    $translateProvider
        .translations('en', {

            // Define all menu elements
            DASHBOARD: 'Dashboard',
            GRAPHS: 'Graphs',
            MAILBOX: 'Mailbox',
            WIDGETS: 'Widgets',
            METRICS: 'Metrics',
            FORMS: 'Forms',
            APPVIEWS: 'App views',
            OTHERPAGES: 'Other pages',
            UIELEMENTS: 'UI elements',
            MISCELLANEOUS: 'Miscellaneous',
            GRIDOPTIONS: 'Grid options',
            TABLES: 'Tables',
            COMMERCE: 'E-commerce',
            GALLERY: 'Gallery',
            MENULEVELS: 'Menu levels',
            ANIMATIONS: 'Animations',
            LANDING: 'Landing page',
            LAYOUTS: 'Layouts',

            // Define some custom text
            WELCOME: 'Welcome Amelia',
            MESSAGEINFO: 'You have 42 messages and 6 notifications.',
            SEARCH: 'Search for something...',

        })
        .translations('es', {

            // Define all menu elements
            DASHBOARD: 'Salpicadero',
            GRAPHS: 'Gráficos',
            MAILBOX: 'El correo',
            WIDGETS: 'Widgets',
            METRICS: 'Métrica',
            FORMS: 'Formas',
            APPVIEWS: 'Vistas app',
            OTHERPAGES: 'Otras páginas',
            UIELEMENTS: 'UI elements',
            MISCELLANEOUS: 'Misceláneo',
            GRIDOPTIONS: 'Cuadrícula',
            TABLES: 'Tablas',
            COMMERCE: 'E-comercio',
            GALLERY: 'Galería',
            MENULEVELS: 'Niveles de menú',
            ANIMATIONS: 'Animaciones',
            LANDING: 'Página de destino',
            LAYOUTS: 'Esquemas',

            // Define some custom text
            WELCOME: 'Bienvenido Amelia',
            MESSAGEINFO: 'Usted tiene 42 mensajes y 6 notificaciones.',
            SEARCH: 'Busca algo ...',
        });

    $translateProvider.preferredLanguage('en');

}]);


app.run(function($rootScope, $state) {
    $rootScope.$state = $state;
});

(function () {
})();

