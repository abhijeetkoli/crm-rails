var appResources = angular.module('appResources', ['rails']);

appResources.factory('Staff', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/staffs',
        name: 'staff',
        pluralName: 'staffs',
        serializer: railsSerializer(function () {
            this.nestedAttribute('user');
        })
    });
}]);

appResources.factory('AppUser', ['railsResourceFactory', function (railsResourceFactory) {
    return railsResourceFactory({
        url: '/app_users',
        name: 'app_user',
        pluralName: 'app_users',
        underscoreParams: true,
    });
}]);

appResources.factory('Dealership', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/dealerships',
        name: 'dealership',
        pluralName: 'dealerships',
        serializer: railsSerializer(function () {
            this.nestedAttribute('staff');
            this.nestedAttribute('customers');
            this.nestedAttribute('vehicles');
            this.nestedAttribute('lots');
        })
    });
}]);


appResources.factory('Customer', ['railsResourceFactory', 'railsSerializer', function (railsResourceFactory, railsSerializer) {
    return railsResourceFactory({
        url: '/customers',
        name: 'customer',
        pluralName: 'customers',
        serializer: railsSerializer(function () {
            this.nestedAttribute('vehicles');
        })
    });
}]);

appResources.factory('Vehicle', ['railsResourceFactory', 'Dealership', 'Customer', function (railsResourceFactory, Dealership, Customer) {
    var resource = railsResourceFactory({
        url: '/vehicles',
        name: 'vehicle',
        pluralName: 'vehicles'
    });
    return resource;
}]);

appResources.factory('VehicleHistory', ['railsResourceFactory', function (railsResourceFactory) {
    return railsResourceFactory({
        url: '/vehicle_histories',
        name: 'vehicle_history'
    });
}]);

appResources.factory("Profile", [
    "Upload", function(Upload) {
        var sendPayload;
        sendPayload = function(formData, method, url) {
            var avatar, options, ref, ref1;
            avatar = (ref = formData.avatar) != null ? ref : [];
            options = {
                url: url,
                method: method,
                file: avatar,
                file_form_data_name: (ref1 = avatar.name) != null ? ref1 : "",
                fields: {
                    app_user: formData.app_user
                }
            };
            return Upload.upload(options);
        };
        return {
            createWithAttachment: function(formData) {
                return sendPayload(formData, "POST", "/app_users.json");
            },
            editWithAttachment: function(formData, recordId) {
                return sendPayload(formData, "PUT", "/app_users/" + recordId + ".json");
            }
        };
    }
]);

appResources.factory("StaffUser", [
    "Upload", function(Upload) {
        var sendPayload;
        sendPayload = function(formData, method, url) {
            var avatar, options, ref, ref1;
            avatar = (ref = formData.avatar) != null ? ref : [];
            options = {
                url: url,
                method: method,
                file: avatar,
                file_form_data_name: (ref1 = avatar.name) != null ? ref1 : "",
                fields: {
                    staff: formData.staff
                }
            };
            return Upload.upload(options);
        };
        return {
            createWithAttachment: function(formData) {
                return sendPayload(formData, "POST", "/staffs.json");
            },
            editWithAttachment: function(formData, recordId) {
                return sendPayload(formData, "PUT", "/staffs/" + recordId + ".json");
            }
        };
    }
]);

appResources.factory("DealershipUpload", [
    "Upload", function(Upload) {
        var sendPayload;
        sendPayload = function(formData, method, url) {
            var logo, options, ref, ref1;
            logo = (ref = formData.logo) != null ? ref : [];
            options = {
                url: url,
                method: method,
                file: logo,
                file_form_data_name: (ref1 = logo.name) != null ? ref1 : "",
                fields: {
                    dealership: formData.dealership
                }
            };
            return Upload.upload(options);
        };
        return {
            createWithAttachment: function(formData) {
                return sendPayload(formData, "POST", "/dealerships.json");
            },
            editWithAttachment: function(formData, recordId) {
                return sendPayload(formData, "PUT", "/dealerships/" + recordId + ".json");
            }
        };
    }
]);

appResources.factory("CustomerUpload", [
    "Upload", function(Upload) {
        var sendPayload;
        sendPayload = function(formData, method, url) {
            var logo, options, ref, ref1;
            logo = (ref = formData.logo) != null ? ref : [];
            options = {
                url: url,
                method: method,
                file: logo,
                file_form_data_name: (ref1 = logo.name) != null ? ref1 : "",
                fields: {
                    customer: formData.customer
                }
            };
            return Upload.upload(options);
        };
        return {
            createWithAttachment: function(formData) {
                return sendPayload(formData, "POST", "/customers.json");
            },
            editWithAttachment: function(formData, recordId) {
                return sendPayload(formData, "PUT", "/customers/" + recordId + ".json");
            }
        };
    }
]);
