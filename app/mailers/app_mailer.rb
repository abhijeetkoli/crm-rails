class AppMailer < Devise::Mailer
 
  def confirmation_instructions(record, token, opts={})
    options = {
      :subject => "Account Verification",
      :email => record.email,
      :name => record.name,
      :global_merge_vars => [
        {
          name: "confirmation_url",
          content: "http://52.88.75.132/users/confirmation?confirmation_token=#{token}"
        },
        {
          name: "name",
          content: record.name
        }
      ],
      :template => "Verification"
    }
    mandrill_send options  
  end
  
  def reset_password_instructions(record, token, opts={})
    options = {
      :subject => "Password Reset",
      :email => record.email,
      :global_merge_vars => [
        {
          name: "reset_password_url",
          content: "http://http://52.88.75.132/reset?reset_password_token=#{token}"
        },
        {
          name: "full_name",
          content: record.full_name
        }
      ],
      :template => "Forgot Password"
    }
    mandrill_send options  
  end
  
  def unlock_instructions(record, token, opts={})
    # code to be added here later
  end

  
  def password_changed_notification(full_name, email)

    options = {
      :subject => "Password Changed",
      :email => email,
      :name => full_name,
      :global_merge_vars => [
        {
          name: "full_name",
          content: full_name
        },
      ],
      :template => "Password Changed Notification"
    }

    mandrill_send options

  end
  
  def mandrill_send(opts={})
    message = { 
      :subject=> "#{opts[:subject]}", 
      :from_name=> "Auto Group",
      :from_email=>"no-reply@autogroup.com",
      :signing_domain =>"autogroup.com",
      :to=>
            [{"name"=>"#{opts[:name]}",
                "email"=>"#{opts[:email]}",
                "type"=>"to"}],
      :global_merge_vars => opts[:global_merge_vars]
      }
    sending = MANDRILL.messages.send_template opts[:template], [], message

    rescue Mandrill::Error => e
      # Mandrill errors are thrown as exceptions
      puts "A mandrill error occurred: #{e.class} - #{e.message}"
      # A mandrill error occurred: Mandrill::UnknownSubaccountError - No subaccount exists with the id 'customer-123'    
      Rails.logger.debug("#{e.class}: #{e.message}")
      raise

  end
  
end