Myapp::Application.routes.draw do

  resources :companies
  resources :staffs
  resources :dealerships
  resources :customers
  resources :vehicles

  devise_for :users, :controllers => {registrations: 'users/registrations', sessions: 'users/sessions', confirmations: 'users/confirmations', :invitations => 'users/invitations'}
  
  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)


  get "pages/lockscreen"
  get "pages/forgot_password"
  get "pages/not_found_error"
  get "pages/internal_server_error"

  get '/app', to: 'app#index', as: 'app'
  get 'pages/login', to: 'pages#login', as: 'pages/login'
  get '/landing', to: 'landing#index', as: 'landing'
  get '/dashboard', to: 'dashboard#index', as: 'dashboard'
  get 'pages/dashboard_google', to: 'pages#dashboard_google', as: 'pages/dashboard_google'
  get 'pages/add_user', to: 'pages#add_user', as: 'pages/add_user'
  get 'pages/staffs', to: 'pages#staffs', as: 'pages/staffs'
  get 'pages/staff', to: 'pages#staff', as: 'pages/staff'
  get 'pages/dealerships', to: 'pages#dealerships', as: 'pages/dealerships'
  get 'pages/dealership', to: 'pages#dealership', as: 'pages/dealership'
  get 'pages/customers', to: 'pages#customers', as: 'pages/customers'
  get 'pages/customer', to: 'pages#customer', as: 'pages/customer'
  get 'pages/vehicles', to: 'pages#vehicles', as: 'pages/vehicles'
  get 'pages/vehicle', to: 'pages#vehicle', as: 'pages/vehicle'
  get 'pages//facebook', to: 'pages#facebook', as: 'pages/facebook'
  get 'pages//heat_map', to: 'pages#heat_map', as: 'pages/heat_map'
  get 'pages/profile', to: 'pages#profile', as: 'pages/profile'
  get 'pages/twitter', to: 'pages#twitter', as: 'pages/twitter'
  get 'pages/virtual_sales_floor', to: 'pages#virtual_sales_floor', as: 'pages/virtual_sales_floor'
  get 'pages/ibox_tools', to: 'pages#ibox_tools', as: 'pages/ibox_tools'

  match 'validations/username', to: 'validations#username', as: 'validations/username', via: [:get, :post]

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
root to: 'landing#index'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
